<?php
/**
 * @file
 * Code for the CSS Alter admin page.
 */

function css_alter_admin() {
	$form = array();

	$form['css_alter'] = array(
		'#type' => 'fieldset',
		'#title' => t('CSS Alter Settings'),
		'#collapsible' => FALSE,
		'#collapsed' => FALSE,
	);

	$css_options = variable_get('css_alter_list', array());
	$css_default = _css_alter_filter_options($css_options);

	// $css_options = array_merge($css_options, $css_default);

	$form['css_alter']['css_alter_scripts'] = array(
		'#title' => t('CSS Scripts'),
		'#description' => t('Lists of CSS being added to the page via normal Drupal functions.'),
		'#type' => 'checkboxes',
		'#default' => $css_default,
		'#options' => array_unique($css_options),
	);

	$form['#submit'][] = 'css_alter_admin_submit';

	return system_settings_form($form);
}

function css_alter_admin_submit($form, $form_state) {

}

function _css_alter_filter_options($list) {
	$scripts = variable_get('css_alter_scripts');
	$options = array();

	foreach ($scripts as $key => $value) {
		if ($value) {
			$options[$key] = '';
		}
	}

	return $options;
}
